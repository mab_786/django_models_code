from django.contrib import admin 
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User

from django.http import  Http404



from django.contrib.auth.models import *

class Manager(BaseUserManager):
    
    def create_user(self, email, username, password = None): # required
        if not email :
            raise ValueError("user must have an email ")
        if not username: 
            raise ValueError("user must have a username ")
        
        
        user = self.model(email = email, username= username, password = password)
        user.set_password(password)
        user.save(using = self._db)
       
        return user 
    
    def create_superuser(self, email, username, password = None): # required
         
        user = self.create_user(email = email, username = username, password=password)
        user.set_password(password)
        
        
        user.is_superuser = True
        user.is_admin = True
        user.is_staff = True
        user.is_active = True
        #user.is_authenticated = True
        
        user.save(using = self._db) 
        return user 
        
    
class MyUser(AbstractBaseUser):
    email = models.EmailField(max_length=40, unique=True)
    username =  models.CharField(max_length = 45, unique=True) 
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    
    
    is_admin = models.BooleanField(default=False) # required for AbstractBaseUser
    is_superuser = models.BooleanField(default=False) # required for AbstractBaseUser
    is_active = models.BooleanField(default =True) # required for AbstractBaseUser
    is_staff = models.BooleanField(default=False) # required for AbstractBaseUser
    
    
    
    objects = Manager()
    USERNAME_FIELD = 'email' # these fields are required but don't write them in required 
    #these username attributes identify a particular user 
     
    REQUIRED_FIELDS = ['username', ]
    
    def __str__(self):
        return self.username 
    
    def has_perm(self, perm, obj = None): # required
        return self.is_admin 
    
    def has_module_perms(self, app_label): # required 
        return True 
    
    
    
# AbstractBaseUser more powerful than Abstractuser
# AbstractUser is django built in 
admin.site.register(MyUser)

class Product(models.Model):
    prodid = models.CharField(primary_key=True, max_length=50) # primary key 
    name = models.CharField(max_length=50, null=True, blank = True)
    qty = models.IntegerField(null=True, blank = True)
    username =  models.ForeignKey('MyUser',  on_delete=models.CASCADE) #foreign key 
    def __str__(self):
        return str(self.prodid) + " "  

admin.site.register(Product)



    
class Feedback(models.Model):
    text = models.TextField()    
    author =  models.ForeignKey('MyUser',  on_delete=models.CASCADE) #foreign key 
    


admin.site.register(Feedback)


class Balance(models.Model):
    username =  models.ForeignKey('MyUser',  on_delete=models.CASCADE) #foreign key
    money = models.IntegerField(null=True, blank = True, default=0)
    recipient = models.CharField(max_length = 1231, null= True, blank = True)    
    trans_amount = models.IntegerField(null = True, blank = True, default=0)
    

    
admin.site.register(Balance)

class Animal(models.Model):
    an_name = models.CharField(max_length = 1231, unique = True)
    qty = models.IntegerField(null=True, blank = True, default=0)    
    indexx =  models.ForeignKey('Quality',  on_delete=models.CASCADE, default = 1) #foreign key
    def __str__(self):
        return self.an_name + " " + str(self.qty)
    class Animal_In_Use(Exception):
        pass
    
admin.site.register(Animal)

class Buy_animal(models.Model):
    an_name = models.CharField(max_length = 1231) 
    qty = models.IntegerField(null=True, blank = True, default=0)
    buyer =  models.ForeignKey('MyUser',  on_delete=models.CASCADE) #foreign key 
    #remember buyer is an object not a char field
    
admin.site.register(Buy_animal)

class Quality(models.Model):
    indexx = models.IntegerField(primary_key = True, default = 1)
    rating = models.CharField(max_length = 1234,null=True, blank=True)
 
admin.site.register(Quality)   

